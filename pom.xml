<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  
	<modelVersion>4.0.0</modelVersion>
  
	<groupId>com.talentum</groupId>
	<artifactId>bank</artifactId>
	<version>1.0.0</version>
	<packaging>pom</packaging>
	<name>Bank</name>
  
	<organization>
		<name>Talentum</name>
	</organization>
  
	<modules>
		<module>bank-model</module>
		<module>bank-manager-api</module>
		<module>bank-manager</module>
		<module>bank-client</module>
		<module>bank-console-client</module>
	</modules>
	
	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<java.version>1.8</java.version>
    
		<maven.compiler.source>${java.version}</maven.compiler.source>
		<maven.compiler.target>${java.version}</maven.compiler.target>
    
		<deps.h2.version>1.4.199</deps.h2.version>
		<deps.slf4j.version>1.7.26</deps.slf4j.version>
		<deps.cucumber.version>4.7.4</deps.cucumber.version>
		<deps.junit.version>4.12</deps.junit.version>
		<deps.junit5.version>5.5.2</deps.junit5.version>
		<deps.junit5-platform.version>1.5.2</deps.junit5-platform.version>
		
		<deps.spring-core.version>5.1.3.RELEASE</deps.spring-core.version>
		<deps.spring-boot.version>2.1.2.RELEASE</deps.spring-boot.version>
    
		<plugin.maven.compiler.version>3.8.0</plugin.maven.compiler.version>
		<plugin.maven.surefire.version>2.22.0</plugin.maven.surefire.version>
		<plugin.maven.failsafe.version>2.22.0</plugin.maven.failsafe.version>
	
	</properties>

	<prerequisites>
		<maven>4.0.0</maven>
	</prerequisites>
 
    
	<dependencyManagement>  
  
		<dependencies>

			<dependency>
				<groupId>com.talentum</groupId>
				<artifactId>bank-model</artifactId>
				<version>${project.version}</version>
			</dependency>

			<dependency>
				<groupId>com.talentum</groupId>
				<artifactId>bank-manager-api</artifactId>
				<version>${project.version}</version>
			</dependency>

			<dependency>
				<groupId>com.talentum</groupId>
				<artifactId>bank-manager</artifactId>
				<version>${project.version}</version>
			</dependency>
		
			<dependency>
				<groupId>com.talentum</groupId>
				<artifactId>bank-client</artifactId>
				<version>${project.version}</version>
			</dependency>

			<dependency>
				<groupId>com.talentum</groupId>
				<artifactId>bank-console-client</artifactId>
				<version>${project.version}</version>
			</dependency>
			
			<dependency>
				<groupId>com.h2database</groupId>
				<artifactId>h2</artifactId>
				<version>${deps.h2.version}</version>
			</dependency>

			<dependency>
				<groupId>org.slf4j</groupId>
				<artifactId>slf4j-api</artifactId>
				<version>${deps.slf4j.version}</version>
			</dependency>

			<dependency>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-starter-data-rest</artifactId>
				<version>${deps.spring-boot.version}</version>
			</dependency>
      
			<dependency>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-starter-jdbc</artifactId>
				<version>${deps.spring-boot.version}</version>
			</dependency>
      
			<dependency>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-starter-web</artifactId>
				<version>${deps.spring-boot.version}</version>
			</dependency>
      
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-web</artifactId>
				<version>${deps.spring-core.version}</version>
			</dependency>

			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-webmvc</artifactId>
				<version>${deps.spring-core.version}</version>
			</dependency>
            
			<dependency>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-dependencies</artifactId>
				<version>${deps.spring-boot.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
 
			<dependency>
				<groupId>io.cucumber</groupId>
				<artifactId>cucumber-java8</artifactId>
				<version>${deps.cucumber.version}</version>
				<scope>test</scope>
			</dependency>
      
			<dependency>
				<groupId>io.cucumber</groupId>
				<artifactId>cucumber-spring</artifactId>
				<version>${deps.cucumber.version}</version>
				<scope>test</scope>
			</dependency>
      
			<dependency>
				<groupId>io.cucumber</groupId>
				<artifactId>cucumber-junit</artifactId>
				<version>${deps.cucumber.version}</version>
				<scope>test</scope>
			</dependency>
      
 			<dependency>
				<groupId>junit</groupId>
				<artifactId>junit</artifactId>
				<version>${deps.junit.version}</version>
				<scope>test</scope>
			</dependency>     

			<dependency>
				<groupId>org.junit.jupiter</groupId>
				<artifactId>junit-jupiter-api</artifactId>
				<version>${deps.junit5.version}</version>
				<scope>test</scope>
			</dependency>

			<dependency>
				<groupId>org.junit.jupiter</groupId>
				<artifactId>junit-jupiter-engine</artifactId>
				<version>${deps.junit5.version}</version>
				<scope>test</scope>
			</dependency>

			<dependency>
				<groupId>org.junit.jupiter</groupId>
				<artifactId>junit-jupiter</artifactId>
				<version>${deps.junit5.version}</version>
				<scope>test</scope>
			</dependency>
			
			<dependency>
				<groupId>org.junit.platform</groupId>
				<artifactId>junit-platform-engine</artifactId>
				<version>${deps.junit5-platform.version}</version>
				<scope>test</scope>
			</dependency>

			<dependency>
			    <groupId>org.junit.platform</groupId>
			    <artifactId>junit-platform-commons</artifactId>
			    <version>${deps.junit5-platform.version}</version>
			    <scope>test</scope>
			</dependency>
			
			<dependency>
				<groupId>org.apache.maven.surefire</groupId>
				<artifactId>surefire-junit47</artifactId>
			</dependency>
      
		</dependencies>
	
	</dependencyManagement>
  
  	
	<build>
  
		<pluginManagement>
		
			<plugins>
		  
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-compiler-plugin</artifactId>
					<version>${plugin.maven.compiler.version}</version>
			  
					<configuration>
						<maven.compiler.source>${maven.compiler.source}</maven.compiler.source>
						<maven.compiler.target>${maven.compiler.target}</maven.compiler.target>
					</configuration>
			  
				</plugin>
			
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-surefire-plugin</artifactId>
					<version>${plugin.maven.surefire.version}</version>
					<dependencies>
						<dependency>
							<groupId>org.apache.maven.surefire</groupId>
							<artifactId>surefire-junit47</artifactId>
							<version>${plugin.maven.surefire.version}</version>
						</dependency>
					</dependencies>	
				</plugin>

				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-failsafe-plugin</artifactId>
					<version>${plugin.maven.failsafe.version}</version>
					<dependencies>
						<dependency>
							<groupId>org.apache.maven.surefire</groupId>
							<artifactId>surefire-junit47</artifactId>
							<version>${plugin.maven.failsafe.version}</version>
						</dependency>
					</dependencies>
				</plugin>
						
			</plugins>
		  
		</pluginManagement>
  	
	</build>
  
</project>