/**
 * 
 */
package com.talentum.bank.model.dao;

import com.talentum.bank.model.entity.Account;

/**
 * @author a.benitoperal
 *
 */
public interface AccountDao {

  /**
   * Creates new account. If the IBAN is not defined, a new unique IBAN will be generated.
   * 
   * @param account
   * 
   * @return
   */
  Account insertAccount(Account account);
  
  /**
   * Returns the account identified by the given IBAN.
   * 
   * @param iban
   * 
   * @return
   */
  Account getAccount(String iban);
  
  /**
   * Updates the account identified by the given IBAN.
   * 
   * @param account
   * 
   * @return
   */
  Account update(Account account);
}
