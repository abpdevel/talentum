/**
 * 
 */
package com.talentum.bank.model.entity;

import java.math.BigDecimal;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * @author a.benitoperal
 *
 */
@Entity
public class Transaction {

  @Id
  private String reference;
  @ManyToOne(optional = false, fetch = FetchType.EAGER)
  private Account account;
  @Column
  private ZonedDateTime date;
  @Column
  private BigDecimal amount;
  @Column
  private BigDecimal fee;
  @Column
  private String description;
  
  /**
   * Default constructor.
   */
  public Transaction() {
    
  }

  /**
   * 
   * 
   * @param reference
   * @param account
   * @param date
   * @param amount
   * @param fee
   * @param description
   */
  public Transaction(
      String reference, 
      Account account, 
      ZonedDateTime date, 
      BigDecimal amount,
      BigDecimal fee, 
      String description) {
    
    this.reference = reference;
    this.account = account;
    this.date = date;
    this.amount = amount;
    this.fee = fee;
    this.description = description;
  }

  /**
   * 
   * 
   * @param reference
   * @param account
   * @param date
   * @param amount
   * @param fee
   * @param description
   */
  public Transaction(
      String reference, 
      BigDecimal amount,
      BigDecimal fee, 
      String description) {
    
    this.reference = reference;
    this.amount = amount;
    this.fee = fee;
    this.description = description;
  }

  public String getReference() {
    return reference;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

  public Account getAccount() {
    return account;
  }

  public void setAccount(Account account) {
    this.account = account;
  }

  public ZonedDateTime getDate() {
    return date;
  }

  public void setDate(ZonedDateTime date) {
    this.date = date;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public BigDecimal getFee() {
    return fee;
  }

  public void setFee(BigDecimal fee) {
    this.fee = fee;
  }
  
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public String toString() {
    return "Transaction [reference=" + reference + ", account=" + account + ", date=" + date
        + ", amount=" + amount + ", fee=" + fee + ", description=" + description + "]";
  }
  
}
