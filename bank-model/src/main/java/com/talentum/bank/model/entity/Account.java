/**
 * 
 */
package com.talentum.bank.model.entity;

import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * @author a.benitoperal
 *
 */
@Entity
public class Account {

  @Id
  private String iban;
  @Column
  private BigDecimal balance;
  @OneToMany(targetEntity = Transaction.class)
  private List<Transaction> transactions;
  /**
   * Default constructor.
   */
  public Account() {

  }

  /**
   * 
   * 
   * @param iban
   */
  public Account(String iban) {
    this.iban = iban;
    this.balance = BigDecimal.ZERO;
  }

  /**
   * 
   * 
   * @param iban
   * @param balance
   */
  public Account(String iban, BigDecimal balance) {
    this.iban = iban;
    this.balance = balance;
  }
  
  public String getIban() {
    return iban;
  }

  public void setIban(String iban) {
    this.iban = iban;
  }

  public BigDecimal getBalance() {
    return balance;
  }

  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }

  public List<Transaction> getTransactions() {
    return transactions;
  }

  public void setTransactions(List<Transaction> transactions) {
    this.transactions = transactions;
  }

  @Override
  public String toString() {
    return "Account [iban=" + iban + ", balance=" + balance + ", transactions=" + transactions + "]";
  }
  
  
}
