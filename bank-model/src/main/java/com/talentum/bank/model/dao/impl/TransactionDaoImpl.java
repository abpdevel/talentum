/**
 * 
 */
package com.talentum.bank.model.dao.impl;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.talentum.bank.model.dao.TransactionDao;
import com.talentum.bank.model.entity.Account;
import com.talentum.bank.model.entity.Transaction;

/**
 * @author a.benitoperal
 *
 */
@Repository
public class TransactionDaoImpl  extends AbstractDao<Transaction, String> implements TransactionDao {

  /**
   * Default constructor. Superclass default constructor initializes entityClass and idClass.
   */
  public TransactionDaoImpl() {
    
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Transaction insertTransaction(Transaction transaction) {

    getEntityManager().persist(transaction);
    
    return transaction;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<Transaction> getTransactions(String iban, boolean asc) {
    
    CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
    CriteriaQuery<Transaction> query = cb.createQuery(getEntityClass());
    
    Root<Transaction> transaction = query.from(getEntityClass());
    Join<Transaction, Account> joinAccount = transaction.join("account");
    query
      .select(transaction)
      .where(cb.equal(joinAccount.get("iban"), iban))
      .orderBy((asc)?cb.asc(transaction.get("amount")):cb.desc(transaction.get("amount")));
    
    return getEntityManager().createQuery(query).getResultList();
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public Transaction getTransaction(String reference) {
    
    return getEntityManager().find(getEntityClass(), reference);
  }
}
