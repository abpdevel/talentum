/**
 * 
 */
package com.talentum.bank.model.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author a.benitoperal
 *
 */
public abstract class AbstractDao<E, I> {

  private Class<E> entityClass;
  private Class<I> idClass;
  
  private EntityManager entityManager;
  
  /**
   * Default constructor initializes entityClass and idClass.
   */
  @SuppressWarnings("unchecked")
  protected AbstractDao() {

    ParameterizedType parameterizedType = (ParameterizedType)this.getClass().getGenericSuperclass();
    Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
    
    this.entityClass = (Class<E>)actualTypeArguments[0];
    this.idClass = (Class<I>)actualTypeArguments[1];
  }

  /**
   * 
   * 
   * @param entityClass
   * @param idClass
   */
  protected AbstractDao(Class<E> entityClass, Class<I> idClass) {
  
    this.entityClass = entityClass;
    this.idClass = idClass;
  }
  
  
  @PersistenceContext
  public void setEntityManager(EntityManager entityManager) {
    this.entityManager = entityManager;
  }
  
  protected EntityManager getEntityManager() {
    return entityManager;
  }
  
  protected Class<E> getEntityClass() {
    return entityClass;
  }

  protected Class<I> getIdClass() {
    return idClass;
  }
  
}
