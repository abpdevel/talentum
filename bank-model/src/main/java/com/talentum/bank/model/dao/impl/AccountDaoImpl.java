/**
 * 
 */
package com.talentum.bank.model.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import com.talentum.bank.model.dao.AccountDao;
import com.talentum.bank.model.entity.Account;

/**
 * @author a.benitoperal
 *
 */
@Repository
public class AccountDaoImpl extends AbstractDao<Account, String> implements AccountDao {

  /**
   * Default constructor. Superclass default constructor initializes entityClass and idClass.
   */
  public AccountDaoImpl() {
    
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Account insertAccount(Account account) {
    
    getEntityManager().persist(account);
    
    return account;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Account getAccount(String iban) {
    
    return getEntityManager().find(getEntityClass(), iban);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Account update(Account account) {
    
    return getEntityManager().merge(account);
  }
  
}
