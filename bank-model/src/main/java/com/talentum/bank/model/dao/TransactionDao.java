/**
 * 
 */
package com.talentum.bank.model.dao;

import java.util.List;
import com.talentum.bank.model.entity.Transaction;

/**
 * @author a.benitoperal
 *
 */
public interface TransactionDao {

  /**
   * 
   * 
   * @param transaction
   * 
   * @return
   */
  Transaction insertTransaction(Transaction transaction);
  
  /**
   * 
   * 
   * @param iban
   * @param asc
   * 
   * @return
   */
  List<Transaction> getTransactions(String iban, boolean asc); 

  /**
   * 
   * 
   * @param reference
   * @return
   */
  Transaction getTransaction(String reference);
}
