/**
 * 
 */
package com.talentum.bank.model.dao;

import java.math.BigDecimal;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Assert;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.talentum.bank.model.dao.impl.AccountDaoImpl;
import com.talentum.bank.model.entity.Account;

/**
 * @author a.benitoperal
 *
 */
@TestMethodOrder(OrderAnnotation.class)
public class AccountDaoTest {
  
  private static final String VALID_ACCOUNT_ID = "ES9820385778983000760236";
  private static final BigDecimal NEW_BALANCE = BigDecimal.valueOf(1000);
  
  private static EntityManagerFactory entityManagerFactory;
  private static EntityManager entityManager;
  private static AccountDaoImpl accountDao;
  
  @BeforeAll
  public static void setUp() {
    entityManagerFactory = Persistence.createEntityManagerFactory("test");
    entityManager = entityManagerFactory.createEntityManager();
    accountDao = new AccountDaoImpl();
    accountDao.setEntityManager(entityManager);
  }

  @AfterAll
  public static void shutDown() {

    entityManager.close();
    entityManagerFactory.close();
  }

  @Test
  @Order(1) 
  public void testGetNonExistingAccount() {

    Account account = accountDao.getAccount(VALID_ACCOUNT_ID);    
    
    Assert.assertNull("Account is null", account);
  }
  
  @Test
  @Order(2) 
  public void testInsertWithIban() {

    entityManager.getTransaction().begin();

    Account newAccount = new Account(VALID_ACCOUNT_ID);

    accountDao.insertAccount(newAccount);

    entityManager.getTransaction().commit();
    
  }

  @Test
  @Order(3) 
  public void testGetExistingEmptyAccount() {

    Account account = accountDao.getAccount(VALID_ACCOUNT_ID);    
    
    Assert.assertNotNull("Account is not null", account);
    Assert.assertEquals(String.format("IBAN is %s.", VALID_ACCOUNT_ID), VALID_ACCOUNT_ID, account.getIban());
    Assert.assertEquals("Account balance is 0.", BigDecimal.ZERO, account.getBalance());
  }

  @Test
  @Order(4) 
  public void testInsertExistingAccount() {

    Assertions.assertThrows(EntityExistsException.class,
        ()-> {
        entityManager.getTransaction().begin();
    
        try {
          Account newAccount = new Account(VALID_ACCOUNT_ID);
      
          accountDao.insertAccount(newAccount);
          
          entityManager.getTransaction().commit();
        } catch (Exception e) {
          entityManager.getTransaction().rollback();
          throw e;
        }
      }
    );
  }

  @Test
  @Order(5) 
  public void testUpdateExistingAccount() {

    entityManager.getTransaction().begin();
  
    Account account = new Account(VALID_ACCOUNT_ID, NEW_BALANCE);
  
    Account updatedAccount = accountDao.update(account);
  
    entityManager.getTransaction().commit();
    
    Assert.assertNotNull("Account is not null", updatedAccount);
    Assert.assertEquals(String.format("IBAN is %s.", VALID_ACCOUNT_ID), VALID_ACCOUNT_ID, account.getIban());
    Assert.assertEquals(String.format("Account balance is %.2f", NEW_BALANCE), NEW_BALANCE, account.getBalance());

  }
}
