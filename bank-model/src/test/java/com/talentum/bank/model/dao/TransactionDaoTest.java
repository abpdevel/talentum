/**
 * 
 */
package com.talentum.bank.model.dao;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;
import org.junit.Assert;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.talentum.bank.model.dao.impl.AccountDaoImpl;
import com.talentum.bank.model.dao.impl.TransactionDaoImpl;
import com.talentum.bank.model.entity.Account;
import com.talentum.bank.model.entity.Transaction;

/**
 * @author a.benitoperal
 *
 */
@TestMethodOrder(OrderAnnotation.class)
public class TransactionDaoTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(TransactionDaoTest.class);
  
  private static final String VALID_ACCOUNT_ID = "ES9820385778983000760236";

  private static final String INVALID_REFERENCE_ID = "00000M";
  
  private static final Transaction[] VALID_TRANSACTIONS = {
      new Transaction("12345A", BigDecimal.valueOf(1000.00), BigDecimal.valueOf(10), "Initial."),
      new Transaction("12346A", BigDecimal.valueOf(-100.00), BigDecimal.valueOf(3.18), "Second.")
  };
  
  private static EntityManagerFactory entityManagerFactory;
  private static EntityManager entityManager;
  private static AccountDaoImpl accountDao;
  private static TransactionDaoImpl transactionDao;
  
  @BeforeAll
  public static void setUp() {
    entityManagerFactory = Persistence.createEntityManagerFactory("test");
    entityManager = entityManagerFactory.createEntityManager();
    accountDao = new AccountDaoImpl();
    accountDao.setEntityManager(entityManager);
    transactionDao = new TransactionDaoImpl();
    transactionDao.setEntityManager(entityManager);
  }

  @AfterAll
  public static void shutDown() {

    entityManager.close();
    entityManagerFactory.close();
  }

  @Test
  @Order(1)
  public void testInsertAccount() {
    
    entityManager.getTransaction().begin();

    Account newAccount = new Account(VALID_ACCOUNT_ID);

    accountDao.insertAccount(newAccount);

    entityManager.getTransaction().commit();
  }
  
  
  
  @Test
  @Order(2) 
  public void testInsertTransactionWithNullAccount() {

    Assertions.assertThrows(PersistenceException.class,
        ()-> {
        entityManager.getTransaction().begin();
    
        try {

          Transaction transaction = new Transaction();

          transaction.setReference(VALID_TRANSACTIONS[0].getReference());
          transaction.setDate(ZonedDateTime.now());
          transaction.setAmount(VALID_TRANSACTIONS[0].getAmount());
          transaction.setFee(VALID_TRANSACTIONS[0].getFee());
          transaction.setDescription(VALID_TRANSACTIONS[0].getDescription());
          
          transactionDao.insertTransaction(transaction);
          
          entityManager.getTransaction().commit();
          
        } catch (Exception e) {
          entityManager.getTransaction().rollback();
          throw e;
        }
      }
    );
  }

  @Test
  @Order(3) 
  public void testInsertInitialTransaction() {

    entityManager.getTransaction().begin();
    
    Account account = accountDao.getAccount(VALID_ACCOUNT_ID);
    
    Transaction transaction = new Transaction();

    transaction.setReference(VALID_TRANSACTIONS[0].getReference());
    transaction.setAccount(account);
    transaction.setDate(ZonedDateTime.now());
    transaction.setAmount(VALID_TRANSACTIONS[0].getAmount());
    transaction.setFee(VALID_TRANSACTIONS[0].getFee());
    transaction.setDescription(VALID_TRANSACTIONS[0].getDescription());
    
    transactionDao.insertTransaction(transaction);
    
    entityManager.getTransaction().commit();
  }

  @Test
  @Order(4) 
  public void testInsertTransactionWithExistingReference() {

    Assertions.assertThrows(PersistenceException.class,
        ()-> {
        entityManager.getTransaction().begin();
    
        try {

          Account account = accountDao.getAccount(VALID_ACCOUNT_ID);
                
          Transaction transaction = new Transaction();

          transaction.setReference(VALID_TRANSACTIONS[0].getReference());
          transaction.setAccount(account);
          transaction.setDate(ZonedDateTime.now());
          transaction.setAmount(VALID_TRANSACTIONS[0].getAmount());
          transaction.setFee(VALID_TRANSACTIONS[0].getFee());
          transaction.setDescription(VALID_TRANSACTIONS[0].getDescription());
          
          transactionDao.insertTransaction(transaction);
          
          entityManager.getTransaction().commit();
          
        } catch (Exception e) {
          entityManager.getTransaction().rollback();
          throw e;
        }
      }
    );
  }
  
  @Test
  @Order(5)
  public void testInsertSecondTransaction() {

    entityManager.getTransaction().begin();
    
    Account account = accountDao.getAccount(VALID_ACCOUNT_ID);
    
    Transaction transaction = new Transaction();

    transaction.setReference(VALID_TRANSACTIONS[1].getReference());
    transaction.setAccount(account);
    transaction.setDate(ZonedDateTime.now());
    transaction.setAmount(VALID_TRANSACTIONS[1].getAmount());
    transaction.setFee(VALID_TRANSACTIONS[1].getFee());
    transaction.setDescription(VALID_TRANSACTIONS[1].getDescription());

    transactionDao.insertTransaction(transaction);
    
    entityManager.getTransaction().commit();
  }

  @Test
  @Order(7) 
  public void testGetNonExistingTransaction() {
  
    Transaction transaction = transactionDao.getTransaction(INVALID_REFERENCE_ID);
    
    Assert.assertNull("Transaction must be null.", transaction);
  }

  @Test
  @Order(8) 
  public void testGetExistingTransactions() {
  
    for (Transaction transaction : VALID_TRANSACTIONS) {
      
      Transaction getTransaction = transactionDao.getTransaction(transaction.getReference());
    
      Assert.assertNotNull("Transaction can not be null.", getTransaction);

      Assert.assertEquals("reference must match", transaction.getReference(), getTransaction.getReference());
    }
  }
  
  @Test
  @Order(9) 
  public void testSearchAccountTransactionAsc() {
  
    List<Transaction> transactions = transactionDao.getTransactions(VALID_ACCOUNT_ID, true);
    
    Assert.assertEquals("The number of transactions must be correct.", VALID_TRANSACTIONS.length, transactions.size());
    
    List<Transaction> orderedTransactions = Arrays.stream(VALID_TRANSACTIONS)
      .sorted((o1, o2) -> {
        return o1.getAmount().compareTo(o2.getAmount());
      })
      .collect(Collectors.toList());
  
    Assert.assertEquals("list must be ordered.", orderedTransactions, transactions);
  }
  
  @Test
  @Order(10) 
  public void testSearchAccountTransactionDesc() {
  
    List<Transaction> transactions = transactionDao.getTransactions(VALID_ACCOUNT_ID, false);
    
    Assert.assertEquals("The number of transactions must be correct.", VALID_TRANSACTIONS.length, transactions.size());

    List<Transaction> orderedTransactions = Arrays.stream(VALID_TRANSACTIONS)
        .sorted((o1, o2) -> {
          return o2.getAmount().compareTo(o1.getAmount());
        })
        .collect(Collectors.toList());
    
    Assert.assertEquals("list must be ordered.", orderedTransactions, transactions);
  }
  
/*

    BigDecimal amount = BigDecimal.valueOf(1000);
    BigDecimal fee = BigDecimal.valueOf(-10);

    Transaction initialTransaction = new Transaction();

    Account currentAccountStatus = entityManager.find(Account.class, "sdfsdf");
    BigDecimal currentBalance = currentAccountStatus.getBalance();
    BigDecimal newBalance = currentBalance.add(amount).add(fee);

    initialTransaction.setReference("aaa");
    initialTransaction.setAccount(currentAccountStatus);
    initialTransaction.setDate(LocalDateTime.now());
    initialTransaction.setAmount(amount);
    initialTransaction.setFee(BigDecimal.valueOf(10));
    initialTransaction.setDescription("Initial");

    currentAccountStatus.setBalance(newBalance);

    entityManager.persist(initialTransaction);

    entityManager.getTransaction().commit();

    System.out.println(entityManager.find(Account.class, "sdfsdf"));
    System.out.println(entityManager.find(Account.class, "sdfsdf").getTransactions());
    System.out.println(entityManager.find(Transaction.class, "aaa"));

    System.out.println("asdasda");*/

  
}
