/**
 * 
 */
package com.talentum.bank.manager.transaction.dto;

import java.io.IOException;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.talentum.bank.manager.transaction.dto.ETransactionChannel;
import com.talentum.bank.manager.transaction.dto.TransactionStatusRequest;

/**
 * @author a.benitoperal
 *
 */
public class TransactionStatusRequestTest {

  private static ObjectMapper OBJECT_MAPPER;
  
  @BeforeClass
  public static void setUp() {
    OBJECT_MAPPER = new ObjectMapper();
  }
  
  @Test
  public void testWithValidChannel() throws IOException {
    
    TransactionStatusRequest transactionStatusRequest = OBJECT_MAPPER.readValue(
        getClass().getClassLoader().getResourceAsStream("json/transactionStatusRequest/withValidChannel.json"), 
        TransactionStatusRequest.class);
    
    Assert.assertEquals("reference does not math.", "12345A", transactionStatusRequest.getReference());
    Assert.assertEquals("channel must be client.", ETransactionChannel.CLIENT , transactionStatusRequest.getChannel());
  }

  @Test
  public void testWithoutChannel() throws IOException {
    
    TransactionStatusRequest transactionStatusRequest = OBJECT_MAPPER.readValue(
        getClass().getClassLoader().getResourceAsStream("json/transactionStatusRequest/withoutChannel.json"), 
        TransactionStatusRequest.class);
    
    Assert.assertEquals("reference must be 12345A.", "12345A", transactionStatusRequest.getReference());
    Assert.assertNull("channel must be null.", transactionStatusRequest.getChannel());
  }
  
  @Test(expected = InvalidFormatException.class)
  public void testWithInvalidChannel() throws IOException {
    
    OBJECT_MAPPER.readValue(
        getClass().getClassLoader().getResourceAsStream("json/transactionStatusRequest/withInvalidChannel.json"), 
        TransactionStatusRequest.class);
    
  }

  @Test(expected = MismatchedInputException.class)
  public void testWithoutReference() throws IOException {
        
    OBJECT_MAPPER.readValue(
        getClass().getClassLoader().getResourceAsStream("json/transactionStatusRequest/withoutReference.json"), 
        TransactionStatusRequest.class);
    
  }
}
