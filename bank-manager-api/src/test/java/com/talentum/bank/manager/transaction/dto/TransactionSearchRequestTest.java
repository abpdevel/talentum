/**
 * 
 */
package com.talentum.bank.manager.transaction.dto;

import java.io.IOException;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.talentum.bank.manager.transaction.dto.ETransactionOrdering;
import com.talentum.bank.manager.transaction.dto.TransactionSearchRequest;

/**
 * @author a.benitoperal
 *
 */
public class TransactionSearchRequestTest {

  private static final String IBAN = "ES9820385778983000760236";
  private static final ETransactionOrdering ORDERING = ETransactionOrdering.ASCENDING;
  
  private static ObjectMapper OBJECT_MAPPER;
  
  @BeforeClass
  public static void setUp() {
    OBJECT_MAPPER = new ObjectMapper();
  }
  
  @Test
  public void testWithoutOrdering() throws IOException {
    
    TransactionSearchRequest transactionSearchRequest = OBJECT_MAPPER.readValue(
        getClass().getClassLoader().getResourceAsStream("json/transactionSearchRequest/withoutOrdering.json"), 
        TransactionSearchRequest.class);
    
    Assert.assertEquals("iban must math.", IBAN, transactionSearchRequest.getIban());
    Assert.assertEquals("ordering must be none.", ETransactionOrdering.NONE , transactionSearchRequest.getOrdering());
  }

  @Test
  public void testWithOrdering() throws IOException {
    
    TransactionSearchRequest transactionSearchRequest = OBJECT_MAPPER.readValue(
        getClass().getClassLoader().getResourceAsStream("json/transactionSearchRequest/withOrdering.json"), 
        TransactionSearchRequest.class);
    
    Assert.assertEquals("reference must math.", IBAN, transactionSearchRequest.getIban());
    Assert.assertEquals("ordering must be none.", ORDERING , transactionSearchRequest.getOrdering());
  }
  
  @Test(expected = MismatchedInputException.class)
  public void testWithoutReference() throws IOException {
        
    OBJECT_MAPPER.readValue(
        getClass().getClassLoader().getResourceAsStream("json/transactionSearchRequest/withoutReference.json"), 
        TransactionSearchRequest.class);
    
  }

  @Test(expected = MismatchedInputException.class)
  public void testWithInvalidOrdering() throws IOException {

    OBJECT_MAPPER.readValue(
        getClass().getClassLoader().getResourceAsStream("json/transactionSearchRequest/withInvalidOrdering.json"), 
        TransactionSearchRequest.class);
  }

  @Test(expected = MismatchedInputException.class)
  public void testWithUnknownOrdering() throws IOException {

    OBJECT_MAPPER.readValue(
        getClass().getClassLoader().getResourceAsStream("json/transactionSearchRequest/withUnknownOrdering.json"), 
        TransactionSearchRequest.class);
  }
}
