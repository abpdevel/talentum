/**
 * 
 */
package com.talentum.bank.manager.account.dto;

import java.io.IOException;
import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author a.benitoperal
 *
 */
public class AccountUpdateResponseTest {

  private static final String IBAN = "ES9820385778983000760236";
  private static final BigDecimal BALANCE = BigDecimal.valueOf(193.38);
  
  private static ObjectMapper OBJECT_MAPPER;
  
  @BeforeClass
  public static void setUp() {
    OBJECT_MAPPER = new ObjectMapper();
  }
  
  @Test
  public void testAccountUpdateResponse() throws IOException {
    
    AccountUpdateResponse accountUpdateResponse = OBJECT_MAPPER.readValue(
          getClass().getClassLoader().getResourceAsStream("json/accountUpdateResponse/valid.json"), 
          AccountUpdateResponse.class);
      
    Assert.assertEquals("iban must match.", IBAN, accountUpdateResponse.getIban());
    Assert.assertEquals("balance must match.", BALANCE, accountUpdateResponse.getBalance());

  }
}
