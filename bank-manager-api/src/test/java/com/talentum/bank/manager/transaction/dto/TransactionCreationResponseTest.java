/**
 * 
 */
package com.talentum.bank.manager.transaction.dto;

import java.io.IOException;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author a.benitoperal
 *
 */
public class TransactionCreationResponseTest {

  private static final String REFERENCE = "12345A";
  
  private static ObjectMapper OBJECT_MAPPER;
  
  @BeforeClass
  public static void setUp() {
    OBJECT_MAPPER = new ObjectMapper();
  }
  
  @Test
  public void testTransactionCreationResponse() throws IOException {
    
    TransactionCreationResponse transactionCreationResponse = OBJECT_MAPPER.readValue(
          getClass().getClassLoader().getResourceAsStream("json/transactionCreationResponse/valid.json"), 
          TransactionCreationResponse.class);
      
    Assert.assertEquals("reference must match.", REFERENCE, transactionCreationResponse.getReference());

  }
}
