/**
 * 
 */
package com.talentum.bank.manager.account.dto;

import java.io.IOException;
import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author a.benitoperal
 *
 */
public class AccountResponseTest {

  private static final String IBAN = "ES9820385778983000760236";
  private static final BigDecimal BALANCE = BigDecimal.valueOf(193.38);
  
  private static ObjectMapper OBJECT_MAPPER;
  
  @BeforeClass
  public static void setUp() {
    OBJECT_MAPPER = new ObjectMapper();
  }
  
  @Test
  public void testAccountResponse() throws IOException {
    
    AccountResponse accountResponse = OBJECT_MAPPER.readValue(
          getClass().getClassLoader().getResourceAsStream("json/accountResponse/valid.json"), 
          AccountResponse.class);
      
    Assert.assertEquals("iban must match.", IBAN, accountResponse.getIban());
    Assert.assertEquals("balance must match.", BALANCE, accountResponse.getBalance());

  }
}
