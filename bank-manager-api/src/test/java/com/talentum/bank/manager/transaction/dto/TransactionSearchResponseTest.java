/**
 * 
 */
package com.talentum.bank.manager.transaction.dto;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.talentum.bank.manager.transaction.dto.TransactionData;
import com.talentum.bank.manager.transaction.dto.TransactionSearchResponse;

/**
 * @author a.benitoperal
 *
 */
public class TransactionSearchResponseTest {

  private static final String REFERENCE = "12345A";
  private static final String IBAN = "ES9820385778983000760236";
  private static final String DATE = "2019-07-16T16:55:42.005Z";
  private static final BigDecimal AMOUNT = BigDecimal.valueOf(193.38);
  private static final BigDecimal FEE = BigDecimal.valueOf(3.18);
  private static final String DESCRIPTION = "Restaurant payment";
  
  private static ObjectMapper OBJECT_MAPPER;
  
  @BeforeClass
  public static void setUp() {
    OBJECT_MAPPER = new ObjectMapper();
  }
  
  @Test
  public void testEmptyResponse() throws IOException {
      
    TransactionSearchResponse transactionSearchResponse = OBJECT_MAPPER.readValue(
          getClass().getClassLoader().getResourceAsStream("json/transactionSearchResponse/empty.json"), 
          TransactionSearchResponse.class);
      
    Assert.assertTrue("list must be empty.", transactionSearchResponse.getTransactions().isEmpty());
  }

  @Test
  public void testNullResponse() throws IOException {
      
    TransactionSearchResponse transactionSearchResponse = OBJECT_MAPPER.readValue(
          getClass().getClassLoader().getResourceAsStream("json/transactionSearchResponse/null.json"), 
          TransactionSearchResponse.class);
      
    Assert.assertTrue("list must be empty.", transactionSearchResponse.getTransactions().isEmpty());
  }

  @Test
  public void testListResponse() throws IOException {
      
    TransactionSearchResponse transactionSearchResponse = OBJECT_MAPPER.readValue(
          getClass().getClassLoader().getResourceAsStream("json/transactionSearchResponse/list.json"), 
          TransactionSearchResponse.class);
      
    List<TransactionData> transactions = transactionSearchResponse.getTransactions();
    
    Assert.assertEquals("list must contain one transaction.", 1,  transactions.size());
    
    TransactionData transactionData = transactions.get(0);
    
    Assert.assertEquals(String.format("reference must be %s.", REFERENCE), REFERENCE, transactionData.getReference());
    Assert.assertEquals(String.format("iban must be %s.", IBAN), IBAN, transactionData.getIban());
    Assert.assertEquals(String.format("date must be %s.", DATE), DATE, transactionData.getStringDate());
    Assert.assertEquals(String.format("amount must be %.2f.", AMOUNT), AMOUNT, transactionData.getAmount());
    Assert.assertEquals(String.format("fee must be %.2f.", FEE), FEE, transactionData.getFee());
    Assert.assertEquals(String.format("description must be %s.", DESCRIPTION), DESCRIPTION, transactionData.getDescription());

  }
}
