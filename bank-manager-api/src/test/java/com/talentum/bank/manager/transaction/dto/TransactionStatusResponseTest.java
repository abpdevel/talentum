/**
 * 
 */
package com.talentum.bank.manager.transaction.dto;

import java.io.IOException;
import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.talentum.bank.manager.transaction.dto.ETransactionStatus;
import com.talentum.bank.manager.transaction.dto.TransactionStatusResponse;

/**
 * @author a.benitoperal
 *
 */
public class TransactionStatusResponseTest {

  private static ObjectMapper OBJECT_MAPPER;
  
  @BeforeClass
  public static void setUp() {
    OBJECT_MAPPER = new ObjectMapper();
  }
  
  @Test
  public void testTransactionStatusResponse() throws IOException {
    
    TransactionStatusResponse transactionStatusResponse = OBJECT_MAPPER.readValue(
          getClass().getClassLoader().getResourceAsStream("json/transactionStatusResponse/valid.json"), 
          TransactionStatusResponse.class);
      
    Assert.assertEquals("reference does not math.", "12345A", transactionStatusResponse.getReference());
    Assert.assertEquals("status must be invalid.", ETransactionStatus.INVALID, transactionStatusResponse.getStatus());
    Assert.assertEquals("amount must be 23.4.", BigDecimal.valueOf(23.4), transactionStatusResponse.getAmount());
    Assert.assertEquals("fee must be 3.18.", BigDecimal.valueOf(3.18), transactionStatusResponse.getFee());

  }
}
