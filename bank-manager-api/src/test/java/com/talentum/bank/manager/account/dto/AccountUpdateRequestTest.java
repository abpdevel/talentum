/**
 * 
 */
package com.talentum.bank.manager.account.dto;

import java.io.IOException;
import java.math.BigDecimal;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;

/**
 * @author a.benitoperal
 *
 */
public class AccountUpdateRequestTest {

  private static final String IBAN = "ES9820385778983000760236";
  private static final BigDecimal BALANCE = BigDecimal.valueOf(193.38);
  
  private static ObjectMapper OBJECT_MAPPER;
  
  @BeforeClass
  public static void setUp() {
    OBJECT_MAPPER = new ObjectMapper();
  }
  
  @Test
  public void testWithValidIban() throws IOException {
    
    AccountUpdateRequest accountUpdateRequest = OBJECT_MAPPER.readValue(
        getClass().getClassLoader().getResourceAsStream("json/accountUpdateRequest/withValidIban.json"), 
        AccountUpdateRequest.class);
    
    Assert.assertEquals(String.format("iban must be %s.", IBAN), IBAN, accountUpdateRequest.getIban());
    Assert.assertEquals(String.format("balance must be %.2f.", BALANCE), BALANCE, accountUpdateRequest.getBalance());
  }

  @Test(expected = MismatchedInputException.class)
  public void testWithoutIban() throws IOException {
    
    OBJECT_MAPPER.readValue(
        getClass().getClassLoader().getResourceAsStream("json/accountUpdateRequest/withoutIban.json"), 
        AccountUpdateRequest.class);
    
  }
  
  // TODO
  //@Test(expected = InvalidFormatException.class)
  public void testWithInvalidIban() throws IOException {
        
    OBJECT_MAPPER.readValue(
        getClass().getClassLoader().getResourceAsStream("json/accountUpdateRequest/withInvalidIban.json"), 
        AccountUpdateRequest.class);
    
  }
}
