/**
 * 
 */
package com.talentum.bank.manager.transaction.dto;

import java.io.IOException;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.talentum.bank.manager.transaction.dto.TransactionCreationRequest;

/**
 * @author a.benitoperal
 *
 */
public class TransactionCreationRequestTest {

  private static final String REFERENCE = "12345A";
  private static final String IBAN = "ES9820385778983000760236";
  private static final String DATE = "2019-07-16T16:55:42.005Z";
  private static final BigDecimal AMOUNT = BigDecimal.valueOf(193.38);
  private static final BigDecimal FEE = BigDecimal.valueOf(3.18);
  private static final String DESCRIPTION = "Restaurant payment";
  
  private static ObjectMapper OBJECT_MAPPER;
  
  @BeforeClass
  public static void setUp() {
    OBJECT_MAPPER = new ObjectMapper();
  }
  
  @Test
  public void testValid() throws IOException {
        
    TransactionCreationRequest transactionCreationRequest = OBJECT_MAPPER.readValue(
          getClass().getClassLoader().getResourceAsStream("json/transactionCreationRequest/valid.json"), 
          TransactionCreationRequest.class);
    
    Assert.assertEquals(String.format("reference must be %s.", REFERENCE), REFERENCE, transactionCreationRequest.getReference());
    Assert.assertEquals(String.format("iban must be %s.", IBAN), IBAN, transactionCreationRequest.getIban());
    Assert.assertEquals(String.format("date must be %s.", DATE), DATE, transactionCreationRequest.getStringDate());
    Assert.assertEquals(String.format("amount must be %.2f.", AMOUNT), AMOUNT, transactionCreationRequest.getAmount());
    Assert.assertEquals(String.format("fee must be %.2f.", FEE), FEE, transactionCreationRequest.getFee());
    Assert.assertEquals(String.format("description must be %s.", DESCRIPTION), DESCRIPTION, transactionCreationRequest.getDescription());
  }
  
  @Test(expected = MismatchedInputException.class)
  public void testWithoutAmount() throws IOException {
      
    OBJECT_MAPPER.readValue(
          getClass().getClassLoader().getResourceAsStream("json/transactionCreationRequest/withoutAmount.json"), 
          TransactionCreationRequest.class);
  }

  @Test(expected = MismatchedInputException.class)
  public void testWithoutIban() throws IOException {
    
    OBJECT_MAPPER.readValue(
          getClass().getClassLoader().getResourceAsStream("json/transactionCreationRequest/withoutIban.json"), 
          TransactionCreationRequest.class);
  }
}
