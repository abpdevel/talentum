/**
 * 
 */
package com.talentum.bank.manager.account.dto;

import java.io.IOException;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;

/**
 * @author a.benitoperal
 *
 */
public class AccountCreationRequestTest {

  private static final String IBAN = "ES9820385778983000760236";
  
  private static ObjectMapper OBJECT_MAPPER;
  
  @BeforeClass
  public static void setUp() {
    OBJECT_MAPPER = new ObjectMapper();
  }
  
  @Test
  public void testWithValidIban() throws IOException {
    
    AccountCreationRequest accountCreationRequest = OBJECT_MAPPER.readValue(
        getClass().getClassLoader().getResourceAsStream("json/accountCreationRequest/withValidIban.json"), 
        AccountCreationRequest.class);
    
    Assert.assertEquals(String.format("iban must be %s.", IBAN), IBAN, accountCreationRequest.getIban());
  }

  @Test(expected = MismatchedInputException.class)
  public void testWithoutIban() throws IOException {
    
    OBJECT_MAPPER.readValue(
        getClass().getClassLoader().getResourceAsStream("json/accountCreationRequest/withoutIban.json"), 
        AccountCreationRequest.class);
    
  }
  
  // TODO
  //@Test(expected = InvalidFormatException.class)
  public void testWithInvalidIban() throws IOException {
        
    OBJECT_MAPPER.readValue(
        getClass().getClassLoader().getResourceAsStream("json/accountCreationRequest/withInvalidIban.json"), 
        AccountCreationRequest.class);
    
  }
}
