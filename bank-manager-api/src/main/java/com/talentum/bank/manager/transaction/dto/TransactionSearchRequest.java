/**
 * 
 */
package com.talentum.bank.manager.transaction.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author a.benitoperal
 *
 */
public class TransactionSearchRequest {

  private String iban;
  private ETransactionOrdering ordering;
  
  /**
   * Default constructor.
   */
  public TransactionSearchRequest() {
    this.ordering = ETransactionOrdering.NONE;
  }
  
  /**
   * Sets the given reference and sets ordering to ETransactionOrdering.NONE.
   * 
   * @param iban
   */
  public TransactionSearchRequest(String iban) {
    this.iban = iban;
    this.ordering = ETransactionOrdering.NONE;
  }
  
  /**
   * Sets the attributes of the current request to the given values.
   * 
   * @param reference
   * @param ordering
   */
  @JsonCreator
  public TransactionSearchRequest(
      @JsonProperty(value = "iban", required = true) String iban, 
      @JsonProperty(value = "ordering") ETransactionOrdering ordering) {
    this.iban = iban;
    this.ordering = (ordering != null) ? ordering : ETransactionOrdering.NONE;
  }

  public String getIban() {
    return iban;
  }

  public void setIbane(String iban) {
    this.iban = iban;
  }

  public ETransactionOrdering getOrdering() {
    return ordering;
  }

  public void setOrdering(ETransactionOrdering ordering) {
    this.ordering = (ordering != null) ? ordering : ETransactionOrdering.NONE;
  }

  @Override
  public String toString() {
    return "TransactionSearchRequest [iban=" + iban + ", ordering=" + ordering + "]";
  }
  
}
