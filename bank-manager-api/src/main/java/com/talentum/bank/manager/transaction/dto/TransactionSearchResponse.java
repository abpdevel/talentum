/**
 * 
 */
package com.talentum.bank.manager.transaction.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author a.benitoperal
 *
 */
public class TransactionSearchResponse {

  private List<TransactionData> transactions;
  
  /**
   * Default constructor.
   */
  public TransactionSearchResponse() {
    transactions = new ArrayList<TransactionData>();
  }

  /**
   * 
   * @return
   */
  public List<TransactionData> getTransactions() {
    return new ArrayList<>(transactions);
  }

  /**
   * 
   * @param transactions
   */
  public void setTransactions(List<TransactionData> transactions) {
    this.transactions = (transactions != null) ? new ArrayList<>(transactions) : new ArrayList<>();
  }

  @Override
  public String toString() {
    return "TransactionSearchResponse [transactions=" + transactions + "]";
  }
  
}
