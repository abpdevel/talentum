/**
 * 
 */
package com.talentum.bank.manager.account.dto;

import java.math.BigDecimal;

/**
 * @author a.benitoperal
 *
 */
public class AccountData {

  private String iban;
  private BigDecimal balance;
  
  /**
   * Default constructor.
   */
  public AccountData() {
    
  }

  /**
   * 
   *   
   * @param iban
   * @param balance
   */
  public AccountData(String iban, BigDecimal balance) {

    this.iban = iban;
    this.balance = balance;
  }

  public String getIban() {
    return iban;
  }

  public void setIban(String iban) {
    this.iban = iban;
  }

  public BigDecimal getBalance() {
    return balance;
  }

  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }

  @Override
  public String toString() {
    return "AccountData [iban=" + iban + ", balance=" + balance + "]";
  }
  
}
