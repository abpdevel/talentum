/**
 * 
 */
package com.talentum.bank.manager.account.dto;

import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author a.benitoperal
 *
 */
public final class AccountResponse extends AccountData {

  /**
   * Default constructor.
   */
  public AccountResponse() {
    
  }

  /**
   * 
   * @param iban
   * @param balance
   */
  @JsonCreator
  public AccountResponse(
      @JsonProperty(value = "iban", required = true) String iban,
      @JsonProperty(value = "balance", required = true) BigDecimal balance) {
    
    super(iban, balance);
  }

  @Override
  public String toString() {
    return "AccountResponse [iban=" + getIban() + ", balance=" + getBalance()
        + "]";
  }
  
  
}
