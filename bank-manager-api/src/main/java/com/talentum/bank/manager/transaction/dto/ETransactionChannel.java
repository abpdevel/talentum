/**
 * 
 */
package com.talentum.bank.manager.transaction.dto;

/**
 * @author a.benitoperal
 *
 */
public enum ETransactionChannel {
  CLIENT,
  ATM,
  INTERNAL;
}
