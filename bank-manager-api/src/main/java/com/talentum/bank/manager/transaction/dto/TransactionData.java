/**
 * 
 */
package com.talentum.bank.manager.transaction.dto;

import java.math.BigDecimal;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author a.benitoperal
 *
 */
public class TransactionData {

  public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ISO_ZONED_DATE_TIME;
  
  private String reference;
  private String iban;
  private ZonedDateTime date;
  private BigDecimal amount;
  private BigDecimal fee;
  private String description;
  
  public TransactionData() {
    
  }

  /**
   * 
   * 
   * @param reference
   * @param iban
   * @param date
   * @param amount
   * @param fee
   * @param description
   */
  @JsonCreator
  public TransactionData(
      @JsonProperty(value = "reference") String reference, 
      @JsonProperty(value = "account_iban", required = true) String iban, 
      @JsonProperty(value = "date") String date,
      @JsonProperty(value = "amount", required = true) BigDecimal amount, 
      @JsonProperty(value = "fee") BigDecimal fee, 
      @JsonProperty(value = "description") String description) {

    this.reference = reference;
    this.iban = iban;
    this.date = ZonedDateTime.parse(date, FORMATTER);
    this.amount = amount;
    this.fee = fee;
    this.description = description;
  }

  public String getReference() {
    return reference;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

  @JsonProperty(value = "account_iban")
  public String getIban() {
    return iban;
  }

  public void setIban(String iban) {
    this.iban = iban;
  }

  @JsonProperty(value="date")
  public String getStringDate() {
    return date.format(FORMATTER);
  }

  @JsonIgnore
  public ZonedDateTime getDate() {
    return date;
  }
  
  public void setDate(ZonedDateTime date) {
    this.date = date;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public BigDecimal getFee() {
    return fee;
  }

  public void setFee(BigDecimal fee) {
    this.fee = fee;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public String toString() {
    return "TransactionCreationRequest [reference=" + reference + ", iban=" + iban + ", date="
        + date + ", amount=" + amount + ", fee=" + fee + ", description=" + description + "]";
  }
  
}
