/**
 * 
 */
package com.talentum.bank.manager.service.account.service;

import com.talentum.bank.manager.account.dto.AccountCreationRequest;
import com.talentum.bank.manager.account.dto.AccountRequest;
import com.talentum.bank.manager.account.dto.AccountResponse;
import com.talentum.bank.manager.account.dto.AccountUpdateRequest;
import com.talentum.bank.manager.account.dto.AccountUpdateResponse;

/**
 * @author a.benitoperal
 *
 */
public interface AccountService {

  /**
   * 
   * 
   * @param accountCreationRequest
   * 
   * @return
   */
  void createAccount(AccountCreationRequest accountCreationRequest);
  
  /**
   * 
   * 
   * @param accountRequest
   * 
   * @return
   */
  AccountResponse getAccount(AccountRequest accountRequest);
  
  /**
   * 
   * 
   * @param accountUpdateRequest
   * 
   * @return
   */
  AccountUpdateResponse updateAccount(AccountUpdateRequest accountUpdateRequest);
}
