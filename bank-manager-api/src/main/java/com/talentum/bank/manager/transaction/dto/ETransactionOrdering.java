/**
 * 
 */
package com.talentum.bank.manager.transaction.dto;

/**
 * @author a.benitoperal
 *
 */
public enum ETransactionOrdering {

  NONE,
  ASCENDING,
  DESCENDING;
}
