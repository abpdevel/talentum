/**
 * 
 */
package com.talentum.bank.manager.transaction.service;

import com.talentum.bank.manager.transaction.dto.TransactionCreationRequest;
import com.talentum.bank.manager.transaction.dto.TransactionCreationResponse;
import com.talentum.bank.manager.transaction.dto.TransactionSearchRequest;
import com.talentum.bank.manager.transaction.dto.TransactionSearchResponse;
import com.talentum.bank.manager.transaction.dto.TransactionStatusRequest;
import com.talentum.bank.manager.transaction.dto.TransactionStatusResponse;

/**
 * @author a.benitoperal
 *
 */
public interface TransactionService {

  /**
   * 
   * 
   * @param transactionCreationRequest
   * 
   * @return
   */
  TransactionCreationResponse createTransaction(TransactionCreationRequest transactionCreationRequest);
  
  /**
   * 
   * 
   * @param transactionSearchRequest
   * 
   * @return
   */
  TransactionSearchResponse searchTransactions(TransactionSearchRequest transactionSearchRequest);
  
  /**
   * 
   * 
   * @param transactionSTatusRequest
   * 
   * @return
   */
  TransactionStatusResponse getTransactionStatus(TransactionStatusRequest transactionSTatusRequest);

}
