/**
 * 
 */
package com.talentum.bank.manager.account.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author a.benitoperal
 *
 */
public final class AccountCreationRequest {

  private String iban;
  
  public AccountCreationRequest() {
    
  }

  /**
   * 
   */
  @JsonCreator
  public AccountCreationRequest(
      @JsonProperty(value = "iban", required = true) String iban) {
    this.iban = iban;
  }
  
  public String getIban() {
    return iban;
  }

  public void setIban(String iban) {
    this.iban = iban;
  }

  @Override
  public String toString() {
    return "AccountCreationRequest [iban=" + iban + "]";
  }
  
}
