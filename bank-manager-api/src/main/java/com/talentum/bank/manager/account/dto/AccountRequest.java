/**
 * 
 */
package com.talentum.bank.manager.account.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author a.benitoperal
 *
 */
public final class AccountRequest {

  private String iban;
  
  public AccountRequest() {
    
  }

  /**
   * 
   */
  @JsonCreator
  public AccountRequest(
      @JsonProperty(value = "iban", required = true) String iban) {
    this.iban = iban;
  }
  
  public String getIban() {
    return iban;
  }

  public void setIban(String iban) {
    this.iban = iban;
  }

  @Override
  public String toString() {
    return "AccountRequest [iban=" + iban + "]";
  }
  
}
