/**
 * 
 */
package com.talentum.bank.manager.transaction.dto;

import java.math.BigDecimal;

/**
 * @author a.benitoperal
 *
 */
public class TransactionStatusResponse {

  private String reference;
  private ETransactionStatus status;
  private BigDecimal amount;
  private BigDecimal fee;
  
  /**
   * Default constructor.
   */
  public TransactionStatusResponse() {
    
  }
  
  /**
   * 
   * @param reference
   * @param status
   * @param amount
   * @param fee
   */
  public TransactionStatusResponse(
      String reference, 
      ETransactionStatus status, 
      BigDecimal amount,
      BigDecimal fee) {

    this.reference = reference;
    this.status = status;
    this.amount = amount;
    this.fee = fee;
  }



  public String getReference() {
    return reference;
  }
  
  public void setReference(String reference) {
    this.reference = reference;
  }
  
  public ETransactionStatus getStatus() {
    return status;
  }
  
  public void setStatus(ETransactionStatus status) {
    this.status = status;
  }
  
  public BigDecimal getAmount() {
    return amount;
  }
  
  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }
  
  public BigDecimal getFee() {
    return fee;
  }
  
  public void setFee(BigDecimal fee) {
    this.fee = fee;
  }
  
  @Override
  public String toString() {
    return "TransactionStatusResponse [reference=" + reference + ", status=" + status + ", amount="
        + amount + ", fee=" + fee + "]";
  }
  
}
