/**
 * 
 */
package com.talentum.bank.manager.transaction.dto;

/**
 * @author a.benitoperal
 *
 */
public enum ETransactionStatus {
  
  PENDING,
  SETTLED,
  FUTURE,
  INVALID;
}
