/**
 * 
 */
package com.talentum.bank.manager.transaction.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author a.benitoperal
 *
 */
public final class TransactionCreationRequest extends TransactionData {

  /**
   * Default constructor.
   */
  public TransactionCreationRequest() {
    
  }
  
  /**
   * 
   * 
   * @param reference
   * @param iban
   * @param date
   * @param amount
   * @param fee
   * @param description
   */
  @JsonCreator
  public TransactionCreationRequest(
      @JsonProperty(value = "reference") String reference, 
      @JsonProperty(value = "account_iban", required = true) String iban, 
      @JsonProperty(value = "date") String date,
      @JsonProperty(value = "amount", required = true) BigDecimal amount, 
      @JsonProperty(value = "fee") BigDecimal fee, 
      @JsonProperty(value = "description") String description) {

    super(reference, iban, date, amount, fee, description);
  }
}
