/**
 * 
 */
package com.talentum.bank.manager.transaction.dto;

/**
 * @author a.benitoperal
 *
 */
public class TransactionCreationResponse {

  private String reference;
  
  public TransactionCreationResponse() {
    
  }

  public TransactionCreationResponse(String reference) {
    this.reference = reference;
  }

  public String getReference() {
    return reference;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

  @Override
  public String toString() {
    return "TransactionCreationResponse [reference=" + reference + "]";
  }
  
}
