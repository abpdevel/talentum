/**
 * 
 */
package com.talentum.bank.manager.transaction.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author a.benitoperal
 *
 */
public class TransactionStatusRequest {

  private String reference;
  @JsonProperty(required=true)
  private ETransactionChannel channel;
  
  /**
   * Default constructor.
   */
  public TransactionStatusRequest() {
    
  }

  /**
   * 
   * 
   * @param reference
   * @param channel
   */
  @JsonCreator
  public TransactionStatusRequest(
      @JsonProperty(value = "reference", required=true)String reference, 
      @JsonProperty(value = "channel") ETransactionChannel channel) {
    
    this.reference = reference;
    this.channel = channel;
  }

  public String getReference() {
    return reference;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

  public ETransactionChannel getChannel() {
    return channel;
  }

  public void setChannel(ETransactionChannel channel) {
    this.channel = channel;
  }

  @Override
  public String toString() {
    return "TransactionStatusRequest [reference=" + reference + ", channel=" + channel + "]";
  }
  
}
