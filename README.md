
La orden
 mvn clean install
 
Permite construir y publicar en el repositorio local de Maven los paquetes que forman la aplicación.

En la fase de test del módulo bank-manager se despliega la aplicación y se ejecutan los test de aceptación basados en Cucumber.

Se incluyen distintos ficheros de feautures en el directorio src/test/resources del mismo módulo.

Al implementar el ejercicio el principal problema que he encontrado es la falta de experiencia con Cucumber y su integración con Spring, además de la limitada disposición de tiempo.

Esto ha supuesto las siguientes limitaciones:
 - Deficiente implementación de los Escenarios. 
 - Falta modularización.
 - Este problema me ha condicionado a la hora de redactar las reglas
 - Error al haber utilizado background para inicializar el sistema, puesto que se ejecuta anteriormente a cada escenario.
 - Falta gestionar errores en servicios y delverlos como HttpCodes en servicios REST mediante ExceptionHandler.
 - Falta generador de id para transacciones y cuentas.
 - Falta implementar validadores de reference, iban y solicitudes correctamente.
 - Falta gestionar concurrencia