/**
 * 
 */
package com.talentum.bank.manager.transaction.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.talentum.bank.manager.transaction.channel.ChannelManager;
import com.talentum.bank.manager.transaction.dto.ETransactionChannel;
import com.talentum.bank.manager.transaction.dto.ETransactionOrdering;
import com.talentum.bank.manager.transaction.dto.TransactionCreationRequest;
import com.talentum.bank.manager.transaction.dto.TransactionCreationResponse;
import com.talentum.bank.manager.transaction.dto.TransactionData;
import com.talentum.bank.manager.transaction.dto.TransactionSearchRequest;
import com.talentum.bank.manager.transaction.dto.TransactionSearchResponse;
import com.talentum.bank.manager.transaction.dto.TransactionStatusRequest;
import com.talentum.bank.manager.transaction.dto.TransactionStatusResponse;
import com.talentum.bank.manager.transaction.service.TransactionService;

import com.talentum.bank.model.dao.AccountDao;
import com.talentum.bank.model.dao.TransactionDao;
import com.talentum.bank.model.entity.Account;
import com.talentum.bank.model.entity.Transaction;

/**
 * @author a.benitoperal
 *
 */
@Service
@org.springframework.transaction.annotation.Transactional
public class TransactionServiceImpl implements TransactionService {

  @Autowired
  private Logger logger;
  
  private AccountDao accountDao;
  private TransactionDao transactionDao;
  
  private Map<ETransactionChannel, ChannelManager> channelManagers;
  
  private ChannelManager defaultChannelManager;
  
  public TransactionServiceImpl() {
    
  }

  @Autowired
  public void setChannelManagers(Map<ETransactionChannel, ChannelManager> channelManagers) {
    
    this.channelManagers = channelManagers;
  }
  
  @Autowired
  @Qualifier("default")
  public void setDefaultChannelManager(ChannelManager defaultChannelManager) {
    this.defaultChannelManager = defaultChannelManager;
  }
  
  /**
   * 
   * 
   * @param accountDao
   */
  @Autowired
  public void setAccountDao(AccountDao accountDao) {
    this.accountDao = accountDao;
  }
  
  /**
   * 
   * 
   * @param accountDao
   */
  @Autowired
  public void setTransactionDao(TransactionDao transactionDao) {
    this.transactionDao = transactionDao;
  }

  @PostConstruct
  public void init() {
    
    for (Map.Entry<ETransactionChannel, ChannelManager> entry : channelManagers.entrySet()) {
      logger.info("channelManager[{}] = {}", 
          entry.getKey(),
          entry.getValue());
    }
    
    logger.info("default channelManager = {}", defaultChannelManager);
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public TransactionCreationResponse createTransaction(TransactionCreationRequest transactionCreationRequest) {
    
    Account account = accountDao.getAccount(transactionCreationRequest.getIban());
    
    if (account == null) {
      return null;
    }
    
    if (BigDecimal.ZERO.compareTo(
          account.getBalance().add(transactionCreationRequest.getAmount())) > 0) {
      return null;
    }
    
    Transaction transaction = new Transaction();
    
    transaction.setReference(transactionCreationRequest.getReference());
    transaction.setAccount(account);
    transaction.setDate(transactionCreationRequest.getDate());
    transaction.setAmount(transactionCreationRequest.getAmount());
    transaction.setFee(transactionCreationRequest.getFee());
    transaction.setDescription(transactionCreationRequest.getDescription());
    
    transaction = transactionDao.insertTransaction(transaction);
    
    return new TransactionCreationResponse(transaction.getReference());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public TransactionStatusResponse getTransactionStatus(
      TransactionStatusRequest transactionStatusRequest) {
    
    if (channelManagers.containsKey(transactionStatusRequest.getChannel())) {
      return channelManagers.get(transactionStatusRequest.getChannel()).getTransactionStatus(transactionStatusRequest);
    }
    
    return defaultChannelManager.getTransactionStatus(transactionStatusRequest);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public TransactionSearchResponse searchTransactions(
      TransactionSearchRequest transactionSearchRequest) {
    
    List<Transaction> transactions = transactionDao.getTransactions(
        transactionSearchRequest.getIban(), 
        ((ETransactionOrdering.DESCENDING.equals(transactionSearchRequest.getOrdering())) ? false :  true));
  
    TransactionSearchResponse transactionSearchResponse = new TransactionSearchResponse();
    
    List<TransactionData> transactionDataList = transactions.stream().map(transaction -> {
      TransactionData transactionData = new TransactionData();
      
      transactionData.setReference(transaction.getReference());
      transactionData.setIban(transaction.getAccount().getIban());
      transactionData.setDate(transaction.getDate());
      transactionData.setAmount(transaction.getAmount());
      transactionData.setFee(transaction.getFee());
      transactionData.setDescription(transaction.getDescription());
      
      return transactionData;
    }).collect(Collectors.toList());
    
    transactionSearchResponse.setTransactions(transactionDataList);
    
    return transactionSearchResponse;
  }
  
}
