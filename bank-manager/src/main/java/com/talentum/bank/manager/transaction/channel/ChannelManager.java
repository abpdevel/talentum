/**
 * 
 */
package com.talentum.bank.manager.transaction.channel;

import com.talentum.bank.manager.transaction.dto.TransactionStatusRequest;
import com.talentum.bank.manager.transaction.dto.TransactionStatusResponse;

/**
 * @author a.benitoperal
 *
 */
public interface ChannelManager {
  
  /**
   * 
   * @param transactionStatusRequest
   * @return
   */
  TransactionStatusResponse getTransactionStatus(TransactionStatusRequest transactionStatusRequest);
}
