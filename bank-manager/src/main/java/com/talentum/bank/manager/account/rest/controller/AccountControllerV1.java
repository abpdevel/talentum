/**
 * 
 */
package com.talentum.bank.manager.account.rest.controller;


import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.talentum.bank.manager.account.dto.AccountCreationRequest;
import com.talentum.bank.manager.account.dto.AccountRequest;
import com.talentum.bank.manager.account.dto.AccountResponse;
import com.talentum.bank.manager.account.dto.AccountUpdateRequest;
import com.talentum.bank.manager.account.dto.AccountUpdateResponse;
import com.talentum.bank.manager.service.account.service.AccountService;

/**
 * @author a.benitoperal
 *
 */
@RestController
@RequestMapping(value="/v1/account")
public class AccountControllerV1 {

  @Autowired
  private Logger logger;
  
  private AccountService accountService;
  
  public AccountControllerV1() {
    
  }
  
  @Autowired
  public void setAccountService(AccountService accountService) {
    this.accountService = accountService;
  }
  
  
  @PutMapping(
      consumes = MediaType.APPLICATION_JSON_VALUE, 
      produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(HttpStatus.CREATED)
  public void create(
      HttpServletRequest request,
      @RequestBody AccountCreationRequest accountCreationRequest) {
    
    logger.info("create - Src = {}, request = {} - ", request, accountCreationRequest);
    
    accountService.createAccount(accountCreationRequest);
  }
  
  @PostMapping(
      consumes = MediaType.APPLICATION_JSON_VALUE, 
      produces = MediaType.APPLICATION_JSON_VALUE)
  public AccountUpdateResponse update(
      HttpServletRequest request,
      @RequestBody AccountUpdateRequest accountUpdateRequest) {
    
    logger.info("update - Src = {}, request = {} - ", request, accountUpdateRequest);
    
    return accountService.updateAccount(accountUpdateRequest);
  }
  
  @PostMapping(
      path = "/get",
      consumes = MediaType.APPLICATION_JSON_VALUE, 
      produces = MediaType.APPLICATION_JSON_VALUE)
  public AccountResponse read(
      HttpServletRequest request,
      @RequestBody AccountRequest accountRequest) {
    
    logger.info("read - Src = {}, request = {} - ", request, accountRequest);
    
    return accountService.getAccount(accountRequest);
  }
  
}
