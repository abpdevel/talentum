/**
 * 
 */
package com.talentum.bank.manager.transaction.channel.impl;


import org.springframework.stereotype.Service;

import com.talentum.bank.manager.transaction.channel.ChannelManager;
import com.talentum.bank.manager.transaction.dto.ETransactionStatus;
import com.talentum.bank.manager.transaction.dto.TransactionStatusRequest;
import com.talentum.bank.manager.transaction.dto.TransactionStatusResponse;

/**
 * @author a.benitoperal
 *
 */
@Service
public class ErrorChannelManagerImpl implements ChannelManager {
  
  public ErrorChannelManagerImpl() {
    
  }

  @Override
  public TransactionStatusResponse getTransactionStatus(
      TransactionStatusRequest transactionStatusRequest) {

    TransactionStatusResponse transactionStatusResponse = new TransactionStatusResponse();
    
    transactionStatusResponse.setReference(transactionStatusRequest.getReference());
    transactionStatusResponse.setStatus(ETransactionStatus.INVALID);
    
    return transactionStatusResponse;
  }

}
