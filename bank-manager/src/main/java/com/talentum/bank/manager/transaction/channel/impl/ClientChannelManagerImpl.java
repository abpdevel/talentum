/**
 * 
 */
package com.talentum.bank.manager.transaction.channel.impl;

import org.springframework.stereotype.Service;

import com.talentum.bank.manager.transaction.channel.ChannelManager;
import com.talentum.bank.manager.transaction.dto.ETransactionStatus;
import com.talentum.bank.manager.transaction.dto.TransactionStatusRequest;
import com.talentum.bank.manager.transaction.dto.TransactionStatusResponse;

import com.talentum.bank.model.entity.Transaction;

/**
 * @author a.benitoperal
 *
 */
@Service
public class ClientChannelManagerImpl extends AbstractChannelManager implements ChannelManager {
  
  public ClientChannelManagerImpl() {
    
  }

  @Override
  protected TransactionStatusResponse getOldTransactionStatus(
      TransactionStatusRequest transactionStatusRequest, Transaction transaction) {
    
    TransactionStatusResponse transactionStatusResponse = new TransactionStatusResponse();
    
    transactionStatusResponse.setReference(transactionStatusRequest.getReference());
    transactionStatusResponse.setStatus(ETransactionStatus.SETTLED);
    transactionStatusResponse.setAmount(transaction.getAmount().subtract(transaction.getFee()));
  
  
    return transactionStatusResponse;
  }

  @Override
  protected TransactionStatusResponse getTodayTransactionStatus(
      TransactionStatusRequest transactionStatusRequest, Transaction transaction) {
    
    TransactionStatusResponse transactionStatusResponse = new TransactionStatusResponse();
    
    transactionStatusResponse.setReference(transactionStatusRequest.getReference());
    transactionStatusResponse.setStatus(ETransactionStatus.PENDING);
    transactionStatusResponse.setAmount(transaction.getAmount().subtract(transaction.getFee()));
    
    return transactionStatusResponse;
  }
  
  @Override
  protected TransactionStatusResponse getFutureTransactionStatus(
      TransactionStatusRequest transactionStatusRequest, Transaction transaction) {
    
    TransactionStatusResponse transactionStatusResponse = new TransactionStatusResponse();
    
    transactionStatusResponse.setReference(transactionStatusRequest.getReference());
    transactionStatusResponse.setStatus(ETransactionStatus.FUTURE);
    transactionStatusResponse.setAmount(transaction.getAmount().subtract(transaction.getFee()));
  
    return transactionStatusResponse;
  }

}
