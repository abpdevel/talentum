/**
 * 
 */
package com.talentum.bank.manager.configuration;

import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import com.talentum.bank.manager.transaction.channel.ChannelManager;
import com.talentum.bank.manager.transaction.channel.impl.AtmChannelManagerImpl;
import com.talentum.bank.manager.transaction.channel.impl.ClientChannelManagerImpl;
import com.talentum.bank.manager.transaction.channel.impl.ErrorChannelManagerImpl;
import com.talentum.bank.manager.transaction.channel.impl.InternalChannelManagerImpl;
import com.talentum.bank.manager.transaction.dto.ETransactionChannel;

/**
 * @author a.benitoperal
 *
 */
@Configuration
public class BankManagerConfiguration {

  
  @Bean
  @Scope("prototype")
  public Logger produceLogger(InjectionPoint injectionPoint) {
      Class<?> classOnWired = injectionPoint.getMember().getDeclaringClass();
      return LoggerFactory.getLogger(classOnWired);
  }
  
  @Bean
  public Map<ETransactionChannel, ChannelManager> getChannelManagers(
      AtmChannelManagerImpl atmChannelManager,
      ClientChannelManagerImpl clientChannelManager,
      InternalChannelManagerImpl internalChannelManager) {
    
    Map<ETransactionChannel, ChannelManager> channelManagers = new HashMap<>();
    
    channelManagers.put(ETransactionChannel.ATM, atmChannelManager);
    channelManagers.put(ETransactionChannel.CLIENT, clientChannelManager);
    channelManagers.put(ETransactionChannel.INTERNAL, internalChannelManager);
    
    return channelManagers;
  }
  
  @Bean
  @Qualifier("default")
  public ChannelManager getDefaultChannelManager() {
    
    return new ErrorChannelManagerImpl();
  }
}
