/**
 * 
 */
package com.talentum.bank.manager.account.service.impl;

import java.math.BigDecimal;
import org.slf4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.talentum.bank.manager.account.dto.AccountCreationRequest;
import com.talentum.bank.manager.account.dto.AccountRequest;
import com.talentum.bank.manager.account.dto.AccountResponse;
import com.talentum.bank.manager.account.dto.AccountUpdateRequest;
import com.talentum.bank.manager.account.dto.AccountUpdateResponse;
import com.talentum.bank.manager.service.account.service.AccountService;
import com.talentum.bank.model.dao.AccountDao;
import com.talentum.bank.model.entity.Account;

/**
 * @author a.benitoperal
 *
 */
@Service
@javax.transaction.Transactional
@org.springframework.transaction.annotation.Transactional
public class AccountServiceImpl implements AccountService {

  @Autowired
  private Logger logger;
  
  private AccountDao accountDao;
  
  /**
   * 
   */
  public AccountServiceImpl() {
    
  }

  /**
   * 
   * 
   * @param accountDao
   */
  @Autowired
  public void setAccountDao(AccountDao accountDao) {
    this.accountDao = accountDao;
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public void createAccount(AccountCreationRequest accountCreationRequest) {
    
    Account account = new Account(accountCreationRequest.getIban(), BigDecimal.ZERO);
    
    account = accountDao.insertAccount(account);
  }


  /**
   * {@inheritDoc}
   */
  @Override
  public AccountResponse getAccount(AccountRequest accountRequest) {

    Account account = accountDao.getAccount(accountRequest.getIban());
    
    if (account == null) {
      return null;
    }
    
    AccountResponse accountResponse = new AccountResponse();
    
    accountResponse.setIban(account.getIban());
    accountResponse.setBalance(account.getBalance());
    
    return accountResponse;
  }


  /**
   * {@inheritDoc}
   */
  @Override
  public AccountUpdateResponse updateAccount(AccountUpdateRequest accountUpdateRequest) {

    Account account = new Account(accountUpdateRequest.getIban(), accountUpdateRequest.getBalance());
    
    account = accountDao.update(account);
    
    AccountUpdateResponse accountUpdateResponse = new AccountUpdateResponse();
    
    accountUpdateResponse.setIban(account.getIban());
    accountUpdateResponse.setBalance(account.getBalance());
    
    return accountUpdateResponse;

  
  
  }

}
