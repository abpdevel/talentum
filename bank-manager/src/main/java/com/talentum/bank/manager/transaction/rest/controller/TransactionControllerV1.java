/**
 * 
 */
package com.talentum.bank.manager.transaction.rest.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.talentum.bank.manager.transaction.dto.TransactionCreationRequest;
import com.talentum.bank.manager.transaction.dto.TransactionCreationResponse;
import com.talentum.bank.manager.transaction.dto.TransactionSearchRequest;
import com.talentum.bank.manager.transaction.dto.TransactionSearchResponse;
import com.talentum.bank.manager.transaction.dto.TransactionStatusRequest;
import com.talentum.bank.manager.transaction.dto.TransactionStatusResponse;
import com.talentum.bank.manager.transaction.service.TransactionService;

/**
 * @author a.benitoperal
 *
 */
@RestController
@RequestMapping(value="/v1/transaction")
public class TransactionControllerV1 {

  @Autowired
  private Logger logger;
  
  private TransactionService transactionService;
  
  public TransactionControllerV1() {
    
  }
  
  @Autowired
  public void setTransactionService(TransactionService transactionService) {
    this.transactionService = transactionService;
  }
  
  
  @PostMapping(
      consumes = MediaType.APPLICATION_JSON_VALUE, 
      produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(HttpStatus.CREATED)
  public TransactionCreationResponse create(
      HttpServletRequest request,
      @RequestBody TransactionCreationRequest transactionCreationRequest) {
    
    logger.info("createTransaction - Src = {}, request = {} - ", request, transactionCreationRequest);
    
    return transactionService.createTransaction(transactionCreationRequest);
  }
  
  @PostMapping(
      path="/search", 
      consumes = MediaType.APPLICATION_JSON_VALUE, 
      produces = MediaType.APPLICATION_JSON_VALUE)
  public TransactionSearchResponse search(
      HttpServletRequest request,
      @RequestBody TransactionSearchRequest transactionSearchRequest) {
    
    logger.info("search - Src = {}, request = {} - ", request, transactionSearchRequest);
    
    return transactionService.searchTransactions(transactionSearchRequest);
  }
  
  @PostMapping(
      path="/status", 
      consumes = MediaType.APPLICATION_JSON_VALUE, 
      produces = MediaType.APPLICATION_JSON_VALUE)
  public TransactionStatusResponse getTransactionStatus(
      HttpServletRequest request,
      @RequestBody TransactionStatusRequest transactionStatusRequest) {
    
    logger.info("getTransactionStatus - Src = {}, request = {} - ", request, transactionStatusRequest);
    
    return transactionService.getTransactionStatus(transactionStatusRequest);
  }
  
}
