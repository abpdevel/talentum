/**
 * 
 */
package com.talentum.bank.manager.transaction.channel.impl;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.talentum.bank.manager.transaction.channel.ChannelManager;
import com.talentum.bank.manager.transaction.dto.ETransactionStatus;
import com.talentum.bank.manager.transaction.dto.TransactionStatusRequest;
import com.talentum.bank.manager.transaction.dto.TransactionStatusResponse;
import com.talentum.bank.model.dao.AccountDao;
import com.talentum.bank.model.dao.TransactionDao;
import com.talentum.bank.model.entity.Transaction;

/**
 * @author a.benitoperal
 *
 */
public abstract class AbstractChannelManager implements ChannelManager {

  @Autowired
  protected Logger logger;
  
  private TransactionDao transactionDao;

  protected AbstractChannelManager() {
    
  }
  
  /**
   * 
   * 
   * @param accountDao
   */
  @Autowired
  public void setTransactionDao(TransactionDao transactionDao) {
    this.transactionDao = transactionDao;
  }
  
  @Override
  public TransactionStatusResponse getTransactionStatus(
      TransactionStatusRequest transactionStatusRequest) {

    Transaction transaction = transactionDao.getTransaction(transactionStatusRequest.getReference());
    
    if (transaction == null) {

      TransactionStatusResponse transactionStatusResponse = new TransactionStatusResponse();
      
      transactionStatusResponse.setReference(transactionStatusRequest.getReference());
      transactionStatusResponse.setStatus(ETransactionStatus.INVALID);
    
      return transactionStatusResponse;
    
    } else {

      ZonedDateTime zonedDateTime = transaction.getDate();
      LocalDate today = LocalDate.now(zonedDateTime.getZone());
      
      if (zonedDateTime.toLocalDate().isBefore(today)) {
        return getOldTransactionStatus(transactionStatusRequest, transaction);
      } else if (zonedDateTime.toLocalDate().isEqual(today)) {
        return getTodayTransactionStatus(transactionStatusRequest, transaction);
      } else {
        return getFutureTransactionStatus(transactionStatusRequest, transaction);
      }
    }
  }

  protected abstract TransactionStatusResponse getOldTransactionStatus(
      TransactionStatusRequest transactionStatusRequest, Transaction transaction);

  protected abstract TransactionStatusResponse getTodayTransactionStatus(
      TransactionStatusRequest transactionStatusRequest, Transaction transaction);
  
  protected abstract TransactionStatusResponse getFutureTransactionStatus(
      TransactionStatusRequest transactionStatusRequest, Transaction transaction);
}
