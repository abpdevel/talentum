/**
 * 
 */
package com.talentum.bank;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author a.benitoperal
 *
 */
@SpringBootApplication
public class BankManagerApp {

  private static final Logger LOGGER = LoggerFactory.getLogger(BankManagerApp.class);

  public static void main(String[] args) {

    LOGGER.info("Starting BankManagerApp...");

    SpringApplication.run(BankManagerApp.class, args);
  }
}
