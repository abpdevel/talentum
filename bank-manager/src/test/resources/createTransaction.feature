Feature: Create Transaction
  An user wants to create a transaction related to an existing account with positive amount

  Background:
    Given An account with non registered IBAN
    When I create the account
    	And I update the balance of new account
    Then A new account is registered
    	And The balance of new account is updated
    	
  Scenario: Account does not exist
    Given IBAN does not exist
    When I create transaction with non existing IBAN
    Then The transaction is not registered in the system

  Scenario: Account exists and the resulting balance is positive
    Given IBAN exists for valid transaction
    	And resulting balance is positive
    When I create transaction with existing IBAN
    Then The transaction is registered in the system

  Scenario: Account exists and the resulting balance is positive and reference exists
    Given Iban exists for valid transaction
    	And resulting balance is positive
    	And reference exists
    When I create transaction
    Then The transaction is registered in the system

  Scenario: Account exists and the resulting balance is negative
    Given Iban exists for big transaction
    	And resulting balance is negative
    When I try to create transaction
    Then The transaction is registered in the system
    
  Scenario: Account exists and the resulting balance is positive and date is in the future
    Given Iban exists for future transaction
    	And resulting balance is positive
    When I create transaction
    Then The transaction is registered in the system

