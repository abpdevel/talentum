Feature: Create Account
  
	Scenario: Create transaction with non existing IBAN
		Given A non existing IBAN
		When I try to create an account And the IBAN is not registered in the system
		Then The system returns the HttpStatus CREATED And the IBAN

	Scenario Create transaction with existing IBAN
		Given An existing IBAN
		When I try to create an account And the IBAN is already registered in the system
		Then The system returns the HttpStatus BAD_REQUEST

	Scenario Create transaction with invalid IBAN
		Given An invalid IBAN
		When I check the status from CLIENT or ATM channel And the IBAN is already registered in the system
		Then The system returns the HttpStatus BAD_REQUEST
			