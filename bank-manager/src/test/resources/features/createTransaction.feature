Feature: Create Transaction
  An user wants to create a transaction related to an existing account with positive amount

  Background:
    Given An account with non registered IBAN
    When I create the account
    	And I update the balance of new account
    Then A new account is registered
    	And The balance of new account is updated
    	
  Scenario: Account does not exist
    Given IBAN does not exist
    When I create transaction with non existing IBAN
    Then The transaction is not registered in the system

