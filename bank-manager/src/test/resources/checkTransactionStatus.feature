Feature: Get Transaction Status
  
	Scenario: Get status for non existing transaction
		Given A transaction that is not stored in our system
		When I check the status from any channel
		Then The system returns the status 'INVALID'
	
	Scenario: Get status for existing transaction	from CLIENT or ATM channel
		Given A transaction that is stored in our system
		When I check the status from CLIENT or ATM channel 
			And the transaction date is before today
		Then The system returns the status 'SETTLED' And the amount substracting the fee

	Scenario: Get status for existing transaction	from INTERNAL channel
		Given A transaction that is stored in our system
		When I check the status from INTERNAL channel 
			And the transaction date is before today
		Then The system returns the status 'SETTLED' 
			And the amount 
			And the fee
		
	Scenario: Get status for existing transaction with date equals to today from CLIENT or ATM channel		
		Given A transaction that is stored in our system
		When I check the status from CLIENT or ATM channel 
			And the transaction date is equals to today
		Then The system returns the status 'PENDING' 
			And the amount substracting the fee

	Scenario: Get status for existing transaction with date equals to today from INTERNAL channel
		Given A transaction that is stored in our system
		When I check the status from INTERNAL channel 
			And the transaction date is equals to today
		Then The system returns the status 'PENDING' 
			And the amount 
			And the fee
		
	Scenario: Get status for existing transaction with date greater than today from CLIENT channel
		Given A transaction that is stored in our system
		When I check the status from CLIENT channel 
			And the transaction date is greater than today
		Then The system returns the status 'FUTURE' 
			And the amount substracting the fee

	Scenario: Get status for existing transaction with date greater than today from ATM channel					
		Given A transaction that is stored in our system
		When I check the status from ATM channel 
			And the transaction date is greater than today
		Then The system returns the status 'PENDING' 
			And the amount substracting the fee

	Scenario: Get status for existing transaction with date greater than today from INTERNAL channel					
		Given A transaction that is stored in our system
		When I check the status from INTERNAL channel 
			And the transaction date is greater than today
		Then: The system returns the status 'FUTURE' 
			And the amount And the fee
	

	