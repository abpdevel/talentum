/**
 * 
 */
package com.talentum.bank;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;
import javax.annotation.PostConstruct;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author a.benitoperal
 *
 */
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class TestConfiguration {

  private static final String SERVER_URL = "http://localhost";
  private static final String ROOT_PATH = "/";

  private String serverUrl;
  private int serverPort;
  private String rootPath;

  private String appUrl;
  
  public TestConfiguration() {
    
    this.serverUrl = TestConfiguration.SERVER_URL;
    this.rootPath = TestConfiguration.ROOT_PATH;
  }
  
  @LocalServerPort
  public void setServerPort(int serverPort) {
    this.serverPort = serverPort;
  }

  public String getAppUrl() {
    return appUrl;
  }

  @PostConstruct
  public void init() {
    this.appUrl = serverUrl + ":" + serverPort +  rootPath;
  }
}
