/**
 * 
 */
package com.talentum.bank;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

import java.io.IOException;
import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.talentum.bank.manager.account.dto.AccountCreationRequest;
import com.talentum.bank.manager.account.dto.AccountRequest;
import com.talentum.bank.manager.account.dto.AccountResponse;
import com.talentum.bank.manager.account.dto.AccountUpdateRequest;
import com.talentum.bank.manager.account.dto.AccountUpdateResponse;
import com.talentum.bank.manager.transaction.dto.TransactionCreationRequest;
import com.talentum.bank.manager.transaction.dto.TransactionCreationResponse;
import com.talentum.bank.manager.transaction.dto.TransactionSearchRequest;
import com.talentum.bank.manager.transaction.dto.TransactionStatusRequest;
import com.talentum.bank.manager.transaction.dto.TransactionStatusResponse;

/**
 * @author a.benitoperal
 *
 */
@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class BankTestClient {

  private static final Logger LOGGER = LoggerFactory.getLogger(BankTestClient.class);
    
  private final String V1_ACCOUNT_CREATION_ENDPOINT = "/v1/account";
  private final String V1_ACCOUNT_READ_ENDPOINT = "/v1/account/get";
  private final String V1_ACCOUNT_UPDATE_ENDPOINT = "/v1/account";
  
  private final String V1_ACCOUNT_CREATION_REQUEST_PATH = "json/accountCreationRequest/";
  private final String V1_ACCOUNT_READ_REQUEST_PATH = "json/accountRequest/";
  private final String V1_ACCOUNT_UPDATE_REQUEST_PATH = "json/accountUpdateRequest/";
  
  private final String V1_TRANSACTION_CREATION_ENDPOINT = "/v1/transaction";
  private final String V1_TRANSACTION_SEARCH_ENDPOINT = "/v1/transaction/search";
  private final String V1_TRANSACTION_STATUS_ENDPOINT = "/v1/transaction/status";
  
  private final String V1_TRANSACTION_CREATION_REQUEST_PATH = "json/transactionCreationRequest/";
  private final String V1_TRANSACTION_SEARCH_REQUEST_PATH = "json/transactionSearchRequest/";
  private final String V1_TRANSACTION_STATUS_REQUEST_PATH = "json/transactionStatusRequest/";

  
  private TestConfiguration testConfiguration;
  
  private RestTemplate restTemplate;
  private ObjectMapper objectMapper;
  
  public BankTestClient() {
    
  }
  
  @Autowired
  public void setTestConfiguration(TestConfiguration testConfiguration) {
    this.testConfiguration = testConfiguration;
  }
  
  @PostConstruct
  public void init() {
    
    this.restTemplate = new RestTemplate();
    this.objectMapper = new ObjectMapper();
    
    LOGGER.info("appUrl = {}", testConfiguration.getAppUrl());
  }
  
  private String getAccountCreationEndPoint() {
    return testConfiguration.getAppUrl() + V1_ACCOUNT_CREATION_ENDPOINT;
  }

  private String getAccountReadEndPoint() {
    return testConfiguration.getAppUrl() + V1_ACCOUNT_READ_ENDPOINT;
  }
  
  private String getAccountUpdateEndPoint() {
    return testConfiguration.getAppUrl() + V1_ACCOUNT_UPDATE_ENDPOINT;
  }
  
  public void createAccount(AccountCreationRequest request) {
    
    LOGGER.info("endpoint = {}", getAccountCreationEndPoint());
    
    restTemplate.put(getAccountCreationEndPoint(), request);
  }
  
  public AccountResponse getAccount(AccountRequest request) {
    
    return restTemplate
        .postForEntity(getAccountReadEndPoint(), request, AccountResponse.class)
        .getBody();
  }
  
  public AccountUpdateResponse updateAccount(AccountUpdateRequest request) {
    
    return restTemplate
        .postForEntity(getAccountUpdateEndPoint(), request, AccountUpdateResponse.class)
        .getBody();
  }
  
  public AccountCreationRequest loadAccountCreationRequest(String requestId) throws IOException {
    
    return objectMapper.readValue(
          getClass().getClassLoader().getResourceAsStream(V1_ACCOUNT_CREATION_REQUEST_PATH + requestId + ".json"), 
          AccountCreationRequest.class);
  }
  
  public AccountRequest loadAccountRequest(String requestId) throws IOException {
    
    return objectMapper.readValue(
          getClass().getClassLoader().getResourceAsStream(V1_ACCOUNT_READ_REQUEST_PATH + requestId + ".json"), 
          AccountRequest.class);
  }

  public AccountUpdateRequest loadAccountUpdateRequest(String requestId) throws IOException {
    
    return objectMapper.readValue(
          getClass().getClassLoader().getResourceAsStream(V1_ACCOUNT_UPDATE_REQUEST_PATH + requestId + ".json"), 
          AccountUpdateRequest.class);
  }
  
  private String getTransactionCreationEndPoint() {
    return testConfiguration.getAppUrl() + V1_TRANSACTION_CREATION_ENDPOINT;
  }

  private String getTransactionSearchEndPoint() {
    return testConfiguration.getAppUrl() + V1_TRANSACTION_SEARCH_ENDPOINT;
  }
  
  private String getTransactionStatusEndPoint() {
    return testConfiguration.getAppUrl() + V1_TRANSACTION_STATUS_ENDPOINT;
  }
  
  public TransactionCreationResponse createTransaction(TransactionCreationRequest request) {
    
    LOGGER.info("endpoint = {}", getTransactionCreationEndPoint());
    
    return restTemplate
        .postForEntity(getTransactionCreationEndPoint(), request, TransactionCreationResponse.class)
        .getBody();
  }
  
  public String searchTransaction(TransactionStatusRequest request) {
    
    return restTemplate
        .postForEntity(getTransactionSearchEndPoint(), request, TransactionCreationResponse.class)
        .getBody().getReference();
  }
  
  public TransactionStatusResponse getTransactionStatus(TransactionStatusRequest request) {
    
    return restTemplate
        .postForEntity(this.getTransactionStatusEndPoint(), request, TransactionStatusResponse.class)
        .getBody();
  }
  
  public TransactionCreationRequest loadTransactionCreationRequest(String requestId) throws IOException {
    
    return objectMapper.readValue(
          getClass().getClassLoader().getResourceAsStream(V1_TRANSACTION_CREATION_REQUEST_PATH + requestId + ".json"), 
          TransactionCreationRequest.class);
  }

  public TransactionSearchRequest loadTransactionSearchRequest(String requestId) throws IOException {
    
    return objectMapper.readValue(
          getClass().getClassLoader().getResourceAsStream(V1_TRANSACTION_SEARCH_REQUEST_PATH + requestId + ".json"), 
          TransactionSearchRequest.class);
  }
  
  public TransactionStatusRequest loadTransactionStatusRequest(String requestId) throws IOException {
    
    return objectMapper.readValue(
          getClass().getClassLoader().getResourceAsStream(V1_TRANSACTION_STATUS_REQUEST_PATH + requestId + ".json"), 
          TransactionStatusRequest.class);
  }

}
