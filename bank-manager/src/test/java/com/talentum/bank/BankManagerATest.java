/**
 * 
 */
package com.talentum.bank;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;


/**
 * @author a.benitoperal
 *
 */
@RunWith(Cucumber.class)
@CucumberOptions(plugin = "pretty", features = {"classpath:features"})
public class BankManagerATest {

}
