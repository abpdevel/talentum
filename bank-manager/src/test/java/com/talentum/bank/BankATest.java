/**
 * 
 */
package com.talentum.bank;

import java.io.IOException;
import java.math.BigDecimal;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.talentum.bank.manager.account.dto.AccountCreationRequest;
import com.talentum.bank.manager.account.dto.AccountRequest;
import com.talentum.bank.manager.account.dto.AccountResponse;
import com.talentum.bank.manager.account.dto.AccountUpdateRequest;
import com.talentum.bank.manager.transaction.dto.ETransactionStatus;
import com.talentum.bank.manager.transaction.dto.ETransactionChannel;
import com.talentum.bank.manager.transaction.dto.TransactionCreationRequest;
import com.talentum.bank.manager.transaction.dto.TransactionCreationResponse;
import com.talentum.bank.manager.transaction.dto.TransactionStatusRequest;
import com.talentum.bank.manager.transaction.dto.TransactionStatusResponse;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.Before;

/**
 * @author a.benitoperal
 *
 */
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
public class BankATest {

  private static final Logger LOGGER = LoggerFactory.getLogger(BankATest.class);
  
  @Autowired
  private BankTestClient bankClient;

  // Background
  private AccountCreationRequest initialAccount;
  private BigDecimal initialBalance = BigDecimal.valueOf(1000.13);
  
  private AccountCreationRequest validAccountRequest;
  private AccountCreationRequest invalidAccountRequest;
  
  private TransactionCreationRequest nonExistingAccountTransactionCreationRequest;
  private TransactionCreationRequest transactionCreationRequest;
  private TransactionCreationRequest bigTransactionCreationRequest;
  private TransactionCreationRequest futureTransactionCreationRequest;

  
  @Before(order=0)
  public void load() throws IOException {
    
    initialAccount = bankClient.loadAccountCreationRequest("initial");

    LOGGER.info("initialAccount = {}", initialAccount);
    
    validAccountRequest = bankClient.loadAccountCreationRequest("withValidIban");
    
    LOGGER.info("validRequest = {}", validAccountRequest);

    invalidAccountRequest = bankClient.loadAccountCreationRequest("withInvalidIban");
    
    LOGGER.info("invalidRequest = {}", invalidAccountRequest);
    
    nonExistingAccountTransactionCreationRequest = bankClient.loadTransactionCreationRequest("nonExistingAccount");
    
    LOGGER.info("request = {}", nonExistingAccountTransactionCreationRequest);
    
    transactionCreationRequest = bankClient.loadTransactionCreationRequest("valid");
    
    LOGGER.info("request = {}", transactionCreationRequest);

    bigTransactionCreationRequest = bankClient.loadTransactionCreationRequest("big");
    
    LOGGER.info("request = {}", bigTransactionCreationRequest);
    
    futureTransactionCreationRequest = bankClient.loadTransactionCreationRequest("future");
    
    LOGGER.info("request = {}", futureTransactionCreationRequest);
  }

  /// Background
  
  @Given("An account with non registered IBAN")
  public void the_account_has_a_non_registered_IBAN() {
    
    AccountResponse accountResponse = bankClient.getAccount(new AccountRequest(transactionCreationRequest.getIban()));
    
    Assert.assertNull("Account must be null.", accountResponse);
  }
  
  @When("I create the account")
  public void i_create_the_account() {
    bankClient.createAccount(initialAccount);
  }
  
  @When("I update the balance of new account")
  public void i_update_the_balance_of_new_account() {
    AccountUpdateRequest accountUpdateRequest = new AccountUpdateRequest(
        initialAccount.getIban(),
        initialBalance);
    
    bankClient.updateAccount(accountUpdateRequest);
  }
  
  @Then("A new account is registered")
  public void a_new_account_is_registered() {

    AccountResponse accountResponse = bankClient.getAccount(new AccountRequest(initialAccount.getIban()));
    
    Assert.assertNotNull("Account can not be null.", accountResponse);
    Assert.assertEquals("the iban must match.", initialAccount.getIban(), accountResponse.getIban());
  }
  
  @Then("The balance of new account is updated")
  public void an_account_with_non_registered_IBAN() {
    
    AccountResponse accountResponse = bankClient.getAccount(new AccountRequest(transactionCreationRequest.getIban()));
    
    Assert.assertNotNull("Account can not be null.", accountResponse);
    Assert.assertEquals("the balance must match.", initialBalance, accountResponse.getBalance());
  }
  
  ///
  /// Create transaction.
  ///
  @Given("IBAN does not exist")
  public void iban_does_not_exist() {
    
    AccountResponse accountResponse = bankClient.getAccount(new AccountRequest(nonExistingAccountTransactionCreationRequest.getIban()));
    
    Assert.assertNull("Account must be null.", accountResponse);
  }
  
  @When("I create transaction with non existing IBAN")
  public void i_create_transaction_with_non_existing_IBAN() {
    
    TransactionCreationResponse response = bankClient.createTransaction(nonExistingAccountTransactionCreationRequest);
    
    LOGGER.info("response = {}", response);
  }

  @Then("The transaction is not registered in the system")
  public void the_transaction_is_not_registerd_in_the_system() {
    
    TransactionStatusResponse transactionStatusResponse = bankClient.getTransactionStatus(
        new TransactionStatusRequest(
            nonExistingAccountTransactionCreationRequest.getReference(),
            ETransactionChannel.INTERNAL));
    
    Assert.assertNotNull("response can not be null", transactionStatusResponse);
    Assert.assertEquals("status must be invalid", ETransactionStatus.INVALID, transactionStatusResponse.getStatus());
  }
  
  @Given("IBAN exists for valid transaction")
  public void iban_exists_for_valid_transaction() {
    
    AccountResponse accountResponse = bankClient.getAccount(new AccountRequest(transactionCreationRequest.getIban()));
  
    Assert.assertNotNull("Account must exists.", accountResponse);
  }

  @Given("resulting balance is positive")
  public void resulting_balance_is_positive() {
    
    AccountResponse accountResponse = bankClient.getAccount(new AccountRequest(transactionCreationRequest.getIban()));
    
    Assert.assertTrue(
        "Resulting balance must be positive.", 
        BigDecimal.ZERO.compareTo(accountResponse.getBalance().add(transactionCreationRequest.getAmount())) <= 0);
  }

  @When("I create transaction with existing IBAN")
  public void i_create_transaction_with_existing_IBAN() {
   
     TransactionCreationResponse response = bankClient.createTransaction(transactionCreationRequest);
     
     LOGGER.info("response = {}", response);
  }

  @Then("The transaction is registered in the system")
  public void the_transaction_is_registered_in_the_system() {

  }
  

  @Given("IBAN exists for big transaction")
  public void iban_exists_for_big_transaction() {
    
    AccountResponse accountResponse = bankClient.getAccount(new AccountRequest(bigTransactionCreationRequest.getIban()));
  
    Assert.assertNotNull("Account must exists.", accountResponse);
  }
  
  @Given("IBAN exists for future transaction")
  public void iban_exists_for_future_transaction() {
    
    AccountResponse accountResponse = bankClient.getAccount(new AccountRequest(futureTransactionCreationRequest.getIban()));
  
    Assert.assertNotNull("Account must exists.", accountResponse);
  }
  

  
  
}
